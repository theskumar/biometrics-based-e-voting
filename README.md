# Biometrics based e-voting System

It is a web based solution that can be used for conducting elections, survey, polls, etc.. With human biometrics using web as platform, it not only provides a highly secure authentication method, but also a anytime, anywhere solution which is highly customizable and simple to use.

Major Components
================

## Fingerprint authentication api 

It is an independent API responsible for fingerprint matching (1:1 or 1:N). It provides REST API as interface for accessing fingerprint enrollment and matching.

## Global Profile Manager

Globally recognized User Profile Manager, provides the user infomation like name, photograph, age, etc. in different formats like JSON, XML, PHP, etc.

## Survey Software

It intergrates fingerprint biometrics and global profile manager with the survey component. It provides easy to use questionaire generator, election data-sheet generator, etc.


Languages used
==============
To work on this project it is expected that develop should have knowledge of one or more of these.
.net Framework, PHP, HTML, CSS, Javascript