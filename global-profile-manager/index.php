<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Global Profile Manager</title>
	<link rel="stylesheet" href="assests/css/style.css"  />
	<link rel="stylesheet" href="assests/css/bootstap.css"  />
	
</head>
<body>
  <div id="container">
    <header>
		<h1>Global Profile Manger</h1>
    </header>
    <div id="main" role="main">

    
	
	</div>
    
	
	<footer>

    </footer>
  </div> <!--! end of #container -->


  <!-- JavaScript at the bottom for fast page loading -->

  <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="assests/js/libs/jquery-1.6.2.min.js"><\/script>')</script>


  <!-- scripts-->
  <script defer src="assests/js/plugins.js"></script>
  <script defer src="assests/js/script.js"></script>
  <!-- end scripts-->
	
</body>
</html>